CXX = g++
DEFINES = -DSTANDALONE
OBJECT_FLAGS = -O2 -Wall
DSTDIR = linux_release
SRCDIR = src
LIBS = -lenet -lz -Lsrc/lua -llua -ldl -lpthread
LANES_SRCDIR = src/lanes/src
LANES_DEFINES = -D_GNU_SOURCE -DNDEBUG
LANES_OBJECT_FLAGS = $(OBJECT_FLAGS)
LANES_INCLUDES = -Isrc/lua


MACHINE := $(shell uname -m)
ifeq ($(MACHINE), x86_64)
	TARGET = $(DSTDIR)/linux_64_server
else
	TARGET = $(DSTDIR)/linux_server
endif

OBJECTS = $(DSTDIR)/crypto.o \
          $(DSTDIR)/log.o \
          $(DSTDIR)/protocol.o \
          $(DSTDIR)/serverms.o \
          $(DSTDIR)/server.o \
          $(DSTDIR)/stream.o \
          $(DSTDIR)/tools.o \
          $(DSTDIR)/wizard.o \
          $(DSTDIR)/lua.o \
          $(DSTDIR)/lua_tmr_library.o \
          $(DSTDIR)/lua_cfg_library.o

LANES_OBJECTS = $(DSTDIR)/lanes_lanes.o \
          $(DSTDIR)/threading_lanes.o \
          $(DSTDIR)/keeper_lanes.o \
          $(DSTDIR)/tools_lanes.o

all: make_DSTDIR $(TARGET)

$(LANES_OBJECTS):
	$(CXX) -c $(LANES_OBJECT_FLAGS) $(LANES_DEFINES) $(LANES_INCLUDES) -o $@ $(LANES_SRCDIR)/$(@F:_lanes.o=.cpp)

$(OBJECTS):
	$(CXX) -c $(OBJECT_FLAGS) $(DEFINES) $(INCLUDES) -o $@ $(SRCDIR)/$(@F:.o=.cpp)

$(TARGET): $(OBJECTS) $(LANES_OBJECTS)
	$(CXX) -o $(TARGET) $(OBJECTS) $(LANES_OBJECTS) -Wl,--no-as-needed $(LIBS) -Wl,-rpath,.

make_DSTDIR:
	mkdir -p $(DSTDIR)

clean:
	rm -f $(OBJECTS) $(LANES_OBJECTS) $(TARGET)
